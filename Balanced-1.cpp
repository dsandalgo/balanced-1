#include <iostream>
#include <bits/stdc++.h>
using namespace std;

bool isBalanced(string args){
    stack<char>s;
    char x;
    for(int i=0; i<args.length(); i++){
        if(args[i] == '(' || args[i] == '{' || args[i] == '['){
           s.push(args[i]); 
           continue;
        }
        if(s.empty())
        return false;
        
        switch(args[i]){
            case ')':
                x = s.top();
                s.pop();
                if(x == '{' || x == '[')
                return false;
            break;
            
            case '}':
                x = s.top();
                s.pop();
                if(x == '(' || x == '[')
                return false;
            break;
            
            case ']':
                x = s.top();
                s.pop();
                if(x == '{' || x == '(')
                return false;
            break;
                
        }
    }
    return (s.empty());
}

int main() {
    string args = "((({[]})))";
    
	if(isBalanced(args))
	    cout<< "Balanced.";
	else
	    cout<< "Not Balanced";
	return 0;
}
